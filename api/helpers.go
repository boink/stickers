package api

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"

	"codeberg.org/boink/stickers/db"
)

func setLoginCookie(w http.ResponseWriter, user db.Users) error {
	buf := make([]byte, 5)
	rand.Read(buf)
	token := fmt.Sprintf("%x", buf)
	cookie := http.Cookie{Name: "login_token", Value: token, Domain: "ashwink.com.np"}
	http.SetCookie(w, &cookie)
	err := db.AddToken(user.Id, token)
	if err != nil {
		return fmt.Errorf("setLoginCookie: can't add token: db.AddToken: %w", err)
	}
	return nil
}

func checkLogin(r *http.Request) (db.Users, bool) {
	cookie, err := r.Cookie("login_token")
	user := db.Users{}
	if err != nil {
		return user, false
	}
	uid, err := db.CheckToken(cookie.Value)
	if err == nil {
		user, err = db.GetUser(uid)
		if err == nil {
			log.Print("logged in by cookie, user: ", user.Username)
		}
	}
	return user, err == nil
}

func basicLogin(w http.ResponseWriter, r *http.Request) (user db.Users, err error) {
	var ok bool
	user.Username , user.Password, ok = r.BasicAuth()
	if !ok {
		err = fmt.Errorf("BasicLogin: no basic login headers")
	}
	user, err = db.CheckLogin(user)
	if err != nil {
		err = fmt.Errorf("BasicLogin: can't login: %v: %w", "db.CheckLogin", err)
	} else {
		err = setLoginCookie(w, user)
	}
	return 
}

func getAllStickersFromDB(ctx *cctx) []db.WholeSticker {
	stkrs, err := db.GetAllStickers()
	if err != nil {
		ctx.err = fmt.Errorf("getAllStickerFD: %v: %w", "db.GetAllStickers", err)
		ctx.status = http.StatusInternalServerError
	}
	return stkrs
}


func getStickerFromRequest(ctx *cctx, r *http.Request) (stkr db.Sticker) {
	if ctx.err != nil {
		return
	}
	stkr.Name = getFromForm(ctx, r.Form, "name")
	stkr.Price = strToInt(ctx, getFromForm(ctx, r.Form, "price"))
	stkr.Description = getFromForm(ctx, r.Form, "description")
	return
}

func getStickerFromDB(ctx *cctx, sid int) (stkr db.Sticker){
	if ctx.err != nil {
		return
	}
	var err error
	stkr, err = db.GetSticker(sid)
	if err != nil {
		ctx.err = fmt.Errorf("getStickerFromDb: %v: %w", "db.GetSticker", err)
		ctx.status = http.StatusBadRequest
	}
	return
}

func getTagsFromRequest(ctx *cctx, r *http.Request) (tgs []db.Tag) {
	if ctx.err != nil {
		return
	}
	for _, tg := range r.Form["tags"] {
		if len(tg) == 0 { continue }
		tgs = append(tgs, db.Tag{Name: tg})
	}
	return
}

func getTagsFromDB(ctx *cctx, sid int) ([]db.Tag) {
	if ctx.err != nil {
		return nil
	}
	tgs, err := db.GetStickerTags(sid)
	if err != nil {
		ctx.err = fmt.Errorf("getTagsFromDB: %v: %w", "db.GetStickerTags", err)
		ctx.status = http.StatusInternalServerError
	}
	return tgs
}

func checkUploadPermission(ctx *cctx, user db.Users){
	if ctx.err != nil {
		return 
	}
	if user.Mode != "admin"  {
		ctx.err = fmt.Errorf("Only admin can add stickers, user is: %v", user)
		ctx.status = http.StatusForbidden
	}else {
		log.Print("checkUploadPerms: user is admin: ", user.Username)
	}
}

func getWholeStickerFromRequest(ctx *cctx, r *http.Request) db.WholeSticker{
	ws := db.WholeSticker{}
	if ctx.err != nil {
		return ws
	}
	ctx.err = r.ParseMultipartForm(threeMiB)
	sticker := getStickerFromRequest(ctx, r)
	tags := getTagsFromRequest(ctx, r)
	if len(tags) == 0 {
		ctx.err = fmt.Errorf("getWholeStickerFR: No tag passed as tags")
		ctx.status = http.StatusBadRequest
		return ws
	}
	images := getImagesFromRequest(ctx, r, "sticker_images")
	if len(images) == 0 {
		ctx.err = fmt.Errorf("getWholeStickerFR: No image passed as sticker_images")
		ctx.status = http.StatusBadRequest
		return ws
	}
	ws = db.WholeSticker{
		Stkr: sticker,
		Tags: tags,
		Images: images,
	}
	return ws
}

func getImagesFromRequest(ctx *cctx, r *http.Request, name string) (imgs []db.Image) {
	if ctx.err != nil {
		return
	}
	if r.MultipartForm == nil || r.MultipartForm.File == nil {
		ctx.err = fmt.Errorf("getImagesFR: no image found: MultipartForm(.file) nil")
		ctx.status = http.StatusBadRequest
		return
	}
	for _, fhs := range r.MultipartForm.File[name] {
		if fhs.Size > MaxUploadBytes {
			ctx.err = fmt.Errorf("getImagesFR: max upload size is %v", MaxUploadBytes)
			ctx.status = http.StatusBadRequest
			return
		}
	}
	// 2 loops, so when the user re-uploads, we don't save old image
	for _, fhs := range r.MultipartForm.File[name] {
		fd, err := fhs.Open()
		if err != nil {
			ctx.err = fmt.Errorf("getImagesFR: cannot open file: %v:%w", "fhs.Open", err)
			ctx.status = http.StatusInternalServerError
			return
		}
		imgs = append(imgs, db.Image{Path: saveFile(ctx, fd)})
	}
	return
}

func getImagesFromDB(ctx *cctx, sid int) ([]db.Image) {
	if ctx.err != nil {
		return nil
	}
	imgs, err := db.GetStickerImages(sid)
	if err != nil {
		ctx.err = fmt.Errorf("getImagesFromDB: %v: %w", "db.GetStickerTags", err)
		ctx.status = http.StatusInternalServerError
	}
	return imgs
}

func getLoggedUser(ctx *cctx, r *http.Request) (user db.Users){
	if ctx.err != nil {
		return
	}
	user, ok := checkLogin(r)
	if !ok {
		ctx.err = fmt.Errorf("Not Logged In")
		ctx.status = 401
	}
	return
}

func saveFile(ctx *cctx, r io.Reader) (path string) {
	if ctx.err != nil {
		return 
	}
	if FileSaveDir == "" {
		ctx.err = fmt.Errorf("saveFile: no directory configured to save in (FileSaveDir)")
		ctx.status = http.StatusInternalServerError
		return
	}
	file, err := os.CreateTemp(FileSaveDir, "user-upload-")
	defer file.Close()
	path = file.Name()
	path = "/" + filepath.Base(FileSaveDir) + "/" + filepath.Base(path)
	if err != nil {
		ctx.err = fmt.Errorf("saveFile: %v: %w", "os.CreateTemp", err)
		ctx.status = http.StatusInternalServerError
		return
	}
	_, err = io.Copy(file, r)
	if err != nil {
		ctx.err = fmt.Errorf("savefile: %v: %w", "io.Copy", err)
		ctx.status = http.StatusInternalServerError
	}
	return
}

func deleteWholeSticker(ctx *cctx, sid int) {
	if ctx.err != nil {
		return
	}
	err := db.DeleteWholeSticker(sid)
	if err != nil {
		ctx.err = fmt.Errorf("deleteWholeSticker: %v: %w", "db.DeleteWholeSticker", err)
		ctx.status = http.StatusInternalServerError
	}
}

func addCart(ctx *cctx, user db.Users, sid int){
	if ctx.err != nil {
		return
	}
	db.AddCart(user, sid)
}

func addWholeSticker(ctx *cctx, ws *db.WholeSticker) {
	if ctx.err != nil {
		return
	}
	defer func(){
		if ctx.err != nil {
			ctx.status = http.StatusInternalServerError
			db.Rollback()
		}
	}()
	var err error
	err = db.StartTransaction()
	if err != nil {
		ctx.err = fmt.Errorf("addsticker: couldn't start transaction: %w", err)
		return;
	}
	ws.Stkr.Id, err = db.AddSticker(ws.Stkr)
	sid := ws.Stkr.Id
	if err != nil {
			ctx.err = fmt.Errorf("addSticker: %v: %w", "db.AddSticker", err)
			return
		}
	for _, tag := range ws.Tags {
		tag.Id, err = db.AddTag(tag)
		if err != nil {
			ctx.err = fmt.Errorf("addSticker: %v: %w", "db.AddTag", err)
			return
		}
		err = db.AddStickerTags(sid, tag.Id)
		if err != nil {
			ctx.err = fmt.Errorf("addSticker: %v: %w", "db.AddStickerTags", err)
			return
		}
	}
	for _, image := range ws.Images {
		image.Id, err = db.AddImage(image)
		if err != nil {
			ctx.err = fmt.Errorf("addSticker: %v: %w", "db.AddImage", err)
			return
		}
		if err = db.AddStickerImages(sid, image.Id); err != nil {
			ctx.err = fmt.Errorf("addSticker: %v: %w", "db.AddStickerImages", err)
			return
		}
	}
	err = db.Commit()
	if err != nil {
		ctx.err = fmt.Errorf("addSticker: %v: %w", "db.Commit", err)
	}
}

func addUser(ctx *cctx, user *db.Users){
	if ctx.err != nil {
		return
	}
	id, err := db.AddUser(*user)
	if err != nil {
		ctx.err = fmt.Errorf("db.addUser: %w", err)
		return
	}
	user.Id = id
}

func checkLength(ctx *cctx, user db.Users){
	if ctx.err != nil {
		return
	}
	if len(user.Username) < 3 || len(user.Password) < 3 {
		ctx.err = fmt.Errorf("username/password length must be >= 3")
		ctx.status = http.StatusTeapot
	}
}

func getUserFromRequest(ctx *cctx, r *http.Request) (user db.Users){
	if ctx.err != nil {
		return
	}
	err := r.ParseForm()
	if err != nil {
		ctx.err = fmt.Errorf("getUserFR: can't parse: %w", err)
		ctx.status = http.StatusBadRequest
	}
	uname := getFromForm(ctx, r.Form, "user")
	pw := getFromForm(ctx, r.Form, "password")
	phone := getFromForm(ctx, r.Form, "phone_no")
	user = db.Users{Username: uname, Password: pw}
	return
}

func getFromForm(ctx *cctx, form url.Values, feild string) string {
	if ctx.err != nil {
		return ""
	}
	v := form[feild]
	if len(v) == 0 {
		ctx.err = fmt.Errorf("getFromForm: no such feild %v", feild)
		ctx.status = http.StatusBadRequest
		return ""
	}
	return v[0]
}

func saveCart(ctx *cctx, user db.Users){
	if ctx.err != nil {
		return
	}
	stickers, err := db.GetCart(user.Id)
	if err != nil {
		ctx.err = fmt.Errorf("saveCart: %v: %w", "db.GetCart", err)
		ctx.status = http.StatusBadRequest
		return
	}
	file, err := os.OpenFile(OrderLocation, os.O_APPEND, 0)
	if err != nil {
		ctx.err = fmt.Errorf("saveCart: %v: %w", "os.OpenFile", err)
		ctx.status = http.StatusInternalServerError
		return
	}
	out, err := json.Marshal(stickers)
	if err != nil {
		ctx.err = fmt.Errorf("saveCart: %v: %w", "json.Marshal", err)
		ctx.status = http.StatusInternalServerError
		return
	}
	fmt.Fprintln(file, string(out))
}

func emptyCart(ctx *cctx, user db.Users){
	if ctx.err != nil {
		return
	}
	ctx.err = db.DelCart(user.Id)
}

func strToInt(ctx *cctx, str string) int {
	if ctx.err != nil {
		return 0
	}
	ret, err := strconv.Atoi(str)
	if err != nil {
		ctx.err = fmt.Errorf("strToInt: converting %v:%v:%w", str, "strconv.Atoi", err)
		ctx.status = http.StatusBadRequest
		return 0
	}
	return ret
}

func replyError(ctx *cctx, w http.ResponseWriter){
	status := ctx.status
	switch status {
	case 0:
		status = http.StatusInternalServerError
	case 401:
		w.Header().Add("WWW-Authenticate", "Basic")
	}
	if ctx.err != nil {
		http.Error(w, ctx.err.Error(), status)
	}else {
		log.Println("replyerror called but ctx.err is false")
	}
}

func replyStruct(ctx *cctx, w http.ResponseWriter, object any) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	if ctx.err != nil {
		replyError(ctx, w)
		return
	}
	log.Print("replyStruct: the object got is ", object)
	resp, err := json.Marshal(object)
	ctx.err = err
	if err != nil {
		replyError(ctx, w)
		return
	}
	log.Print("replyStruct: the json got is ", string(resp))
	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "text/json; charset=utf-8")
	buf := bytes.NewBuffer(resp)
	io.Copy(w, buf)
	
}
