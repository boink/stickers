package api

import (
	"fmt"
	"log"
	"net/http"

	"codeberg.org/boink/stickers/db"
)

type cctx struct {
	err error
	status int
}
const threeMiB = 3 * 1024 * 1024
var FileSaveDir = ""
var MaxUploadBytes int64
var OrderLocation string

type boolReply struct {
	Result string
}
var SuccessReply = boolReply{"Success"}
var FailureReply = boolReply{"Failure"}

func InitHandlers(fileSaveDir string, maxUploadBytes int64, orderLocation string){
	FileSaveDir = fileSaveDir
	MaxUploadBytes = maxUploadBytes
	OrderLocation = orderLocation
}

func MyHandler(w http.ResponseWriter, r *http.Request){
	ctx := &cctx{
		err: fmt.Errorf("not implemented"),
		status: http.StatusNotImplemented,
	}
	replyError(ctx, w)
}

func LoginHandler(w http.ResponseWriter, r *http.Request){
	ctx := &cctx{}
	user, err := basicLogin(w, r)
	if err != nil {
		ctx.err = err
		ctx.status = 401
	}
	replyStruct(ctx, w, user)
}

func NewUserHandler(w http.ResponseWriter, r *http.Request) {
	ctx := &cctx{}
	user := getUserFromRequest(ctx, r)
	checkLength(ctx, user)
	addUser(ctx, &user)
	replyStruct(ctx, w, user)
}

func AddStickerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := &cctx{}
	user := getLoggedUser(ctx, r)
	checkUploadPermission(ctx, user)
	ws := getWholeStickerFromRequest(ctx, r)
	addWholeSticker(ctx, &ws)
	replyStruct(ctx, w, ws)
}

func DeleteStickerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := &cctx{}
	user := getLoggedUser(ctx, r)
	checkUploadPermission(ctx, user)
	sid := strToInt(ctx, getFromForm(ctx, r.Form, "sticker_id"))
	deleteWholeSticker(ctx, sid)
	replyStruct(ctx, w, "Success")
	
}

func GetAllStickersHandler(w http.ResponseWriter, r *http.Request) {
	ctx := &cctx{}
	stkrs := getAllStickersFromDB(ctx)
	replyStruct(ctx, w, stkrs)
}

func GetStickerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := &cctx{}
	r.ParseForm()
	sid := strToInt(ctx, getFromForm(ctx, r.Form, "sticker_id"))
	log.Print("GetStickerHandler: requested whole sticker, id: ", sid)
	ws, err := db.GetWholeSticker(sid)
	if err != nil && ctx.err == nil{
		ctx.err = fmt.Errorf("GetStickerHandler: %v: %w", "db.GetWholeSticker", err)
	}
	replyStruct(ctx, w, ws)
}

func AddToCart(w http.ResponseWriter, r *http.Request) {
	ctx := &cctx{}
	user := getLoggedUser(ctx, r)
	r.ParseForm()
	sid := strToInt(ctx, getFromForm(ctx, r.Form, "sticker_id"))
	addCart(ctx, user, sid)
	replyStruct(ctx, w, SuccessReply)
}

func BuyStickerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := &cctx{}
	user := getLoggedUser(ctx, r)
	saveCart(ctx, user)
	emptyCart(ctx, user)
	replyStruct(ctx, w, SuccessReply)
}
