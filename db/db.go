package db

import (
	"fmt"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB
var err error

type Users struct {
	Id int
	Username, Password, Mode, PhoneNo string
}

type WholeSticker struct {
	Stkr      Sticker
	Tags	  []Tag
	Images	  []Image
}

type Sticker struct {
	Id, Price int
	Name, Description string
}

type Tag struct {
	Id int
	Name string
}

type Image struct {
	Id int
	Path, AltText string
}

func StartTransaction() error {
	_, err := db.Exec("begin transaction")
	return err
}

func Commit() error {
	_, err := db.Exec("commit")
	return err
}

func Rollback() error {
	_, err := db.Exec("rollback")
	return err
}

func CheckLogin(u Users) (Users, error) {
	user, pass := u.Username, u.Password
	query :=
		`
select id, mode from users where username = ? and password = ?
`
	err := db.QueryRow(query, user, pass).Scan(&u.Id, &u.Mode)
	return u, err
}

func AddUser(u Users) (int, error) {
	query := `
insert into users(username, password, mode, phone_no) values (?,?,?,?) returning id
`
	err := db.QueryRow(query, u.Username, u.Password, u.Mode, u.PhoneNo).Scan(&u.Id)
	return u.Id, err
}

func GetUser(uid int) (Users, error) {
	u := Users{Id: uid}
	query := `
select username, password, mode from users where id = ?
`
	err := db.QueryRow(query, uid).Scan(&u.Username, &u.Password, &u.Mode)
	return u, err
}

func AddSticker(s Sticker) (int, error) {
	query := `
insert into stickers(price, name, description) values (?, ?, ?) returning id
`
	err := db.QueryRow(query, s.Price, s.Name, s.Description).Scan(&s.Id)
	return s.Id, err
}

func AddCart(user Users, sid int) error {
	query := `
insert into cart(user_id, sticker_id) values (?,?)
`
	_, err := db.Exec(query, user.Id, sid)
	return err
}

func GetCart(uid int) ([]Sticker, error) {
	query := `
select sticker_id from cart where user_id = ?
`
	rows, err := db.Query(query, uid)
	stkrs := make([]Sticker, 0)
	for err == nil && rows.Next(){
		id := 0
		rows.Scan(&id)
		if err != nil {
			continue
		}
		stkr, err := GetSticker(id)
		if err != nil {
			continue
		}
		stkrs = append(stkrs, stkr)
	}
	if err != nil {
		return stkrs, err
	}else {
		return stkrs, rows.Err()
	}
}

func DelCart(uid int) error {
	query := `
delete from cart where user_id = ?
`
	_, err := db.Exec(query, uid)
	return err
}

func GetSticker(sid int) (sticker Sticker, err error){
	query := `
select id, price, name, description from stickers where id = ?
`
	err = db.QueryRow(query, sid).Scan(&sticker.Id, &sticker.Price, &sticker.Name, &sticker.Description)
	return
}

func GetWholeSticker(sid int) (ws WholeSticker, err error) {
	stkr, tgs, imgs := Sticker{}, make([]Tag,0), make([]Image,0)
	stkr, err = GetSticker(sid)
	if err != nil {
		return
	}
	tgs, err = GetStickerTags(sid)
	if err != nil {
		return
	}
	imgs, err = GetStickerImages(sid)
	ws = WholeSticker{
		Stkr: stkr,
		Tags: tgs,
		Images: imgs,
	}
	return
}

func DeleteWholeSticker(sid int) error {
	err := StartTransaction()
	if err != nil {
		return err
	}
	defer func(){
		if err != nil {
			Rollback()
		}
	}()
	query :=`
delete from stickers where id = ?
`
	_, err = db.Exec(query, sid)
	if err != nil {
		return err
	}
	err = Commit()
	return err
}

func GetAllStickers() ([]WholeSticker, error) {
	query :=
		`
select id from stickers
`
	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	stkrs := make([]WholeSticker, 0)
	for rows.Next() {
		sid := 0
		rows.Scan(&sid)
		ws, _ := GetWholeSticker(sid)
		stkrs = append(stkrs, ws)
	}
	return stkrs, rows.Err()
}

func AddTag(t Tag) (int, error){
	query :=`
insert into tags(name) values (?) returning id
`
	err = db.QueryRow(query, t.Name).Scan(&t.Id)
	return t.Id, err
}

func GetTagFromId(tid int) (tag Tag, err error){
	query := `
select id, name from tags where id = ?
`
	err = db.QueryRow(query, tid).Scan(&tag.Id, &tag.Name)
	return
}

func AddImage(i Image)(int, error){
	query :=`
insert into images(path, alt_text) values (?, ?) returning id
`
	err := db.QueryRow(query, i.Path, i.AltText).Scan(&i.Id)
	return i.Id, err
}

func AddStickerTags(sid, tid int) error {
	query := `
insert into sticker_tags(sticker_id, tag_id) values (?, ?)
`
	_, err := db.Exec(query, sid, tid)
	return err
}

func GetStickerTags(sid int) ([]Tag, error){
	query := `
select id, name from tags join (select tag_id from sticker_tags where sticker_id = ?) as tmp on tmp.tag_id = tags.id
`
	tags := make([]Tag,0)
	rows, err := db.Query(query, sid)
	if err != nil {
		return tags, err
	}
	for rows.Next() {
		tag := Tag{}
		rows.Scan(&tag.Id, &tag.Name)
		tags = append(tags, tag)
	}
	return tags, rows.Err()
}

func AddStickerImages(sid, iid int) error {
	query := `
insert into sticker_images(sticker_id, image_id) values (?, ?)
`
	_, err := db.Exec(query, sid, iid)
	return err
}

func GetStickerImages(sid int) ([]Image, error) {
	query := `
select id, path, alt_text from images join (select image_id from sticker_images where sticker_id = ?) as tmp on tmp.image_id = images.id
`
	images := make([]Image,0)
	rows, err := db.Query(query, sid)
	if err != nil {
		return images, err
	}
	for rows.Next() {
		image := Image{}
		rows.Scan(&image.Id, &image.Path, &image.AltText)
		images = append(images, image)
	}
	return images, rows.Err()
}

func AddToken(uid int, token string) error {
	query := `
insert into login_tokens(user_id, token) values (?,?)
`
	_, err = db.Exec(query, uid, token)
	return err
}
func CheckToken(token string) (id int, err error) {
	query := `
select user_id from login_tokens where token = ?
`
	
	err = db.QueryRow(query, token).Scan(&id)
	return
}

func Init_db() error {
	db, err = sql.Open("sqlite3", "./db.db")
	if err != nil {
		return  err
	}
	newtable :=
		`
begin transaction;
pragma foreign_keys = on;
create table if not exists
users (
id integer primary key autoincrement,
username string unique,
password string,
mode     string,
phone_no string not null
);
` +
`
create table if not exists
stickers (
id integer primary key autoincrement,
price integer,
name string not null,
description string
);
` +
`
create table if not exists
tags (
id integer primary key autoincrement,
name string
);
` +
`
create table if not exists
images (
id integer primary key autoincrement,
path string,
alt_text string
);
` +
`
create table if not exists
sticker_tags (
sticker_id integer,
tag_id integer,
foreign key(sticker_id) references stickers(id) on delete cascade,
foreign key(tag_id) references tags(id) on delete cascade
);
` +
`
create table if not exists
sticker_images (
sticker_id integer,
image_id integer,
foreign key(sticker_id) references stickers(id) on delete cascade,
foreign key (image_id) references images(id) on delete cascade
);
` +
`
create table if not exists
login_tokens (
token string,
user_id integer,
foreign key (user_id) references users(id) on delete cascade
);
`+
`
create table if not exists
cart (
user_id    integer,
sticker_id integer,
foreign key (user_id) references users(id) on delete cascade,
foreign key (sticker_id) references stickers(id) on delete cascade
);
` +
`
commit;
`
	_, err = db.Exec(newtable)
	if err != nil {
		db.Close()
		return fmt.Errorf("cannot create table", err)
	}
	return nil
}

func Close_db(){
	db.Close()
	db = nil
}
