package main

import (
	"log"
	"net/http"
	"os"
	"path/filepath"
	"codeberg.org/boink/stickers/api"
	"codeberg.org/boink/stickers/db"
)

func main(){
	err := db.Init_db()
	if err != nil {
		log.Fatal("could not open db:", err)
	}
	defer db.Close_db()
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal("couldn't get working dir:", err)
	}
	
	api.InitHandlers(filepath.Join(cwd, "uploads"), 5 * 1024 * 1024, filepath.Join(cwd, "orders"))
	
	http.HandleFunc("/", api.MyHandler)
	http.HandleFunc("/api/login", api.LoginHandler)
	http.HandleFunc("/api/signup", api.NewUserHandler)
	http.HandleFunc("/api/sticker/add", api.AddStickerHandler)
	http.HandleFunc("/api/sticker/get", api.GetStickerHandler)
	http.HandleFunc("/api/sticker/del", api.DeleteStickerHandler)
	http.HandleFunc("/api/sticker/buy", api.BuyStickerHandler)
	http.HandleFunc("/api/sticker/cart-add", api.AddToCart)
	http.HandleFunc("/api/sticker/all", api.GetAllStickersHandler)
	http.Handle("/uploads/", http.StripPrefix("/uploads/", http.FileServer(http.Dir("uploads"))))
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	log.Print("listening")
	log.Fatal(http.ListenAndServe(":80", nil))
}
